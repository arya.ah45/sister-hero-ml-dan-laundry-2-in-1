<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRekapsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rekaps', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamp('tanggal_pengajuan'); 
            $table->string('nama_pegawai',70);                
            $table->string('keluhan');   
            $table->string('kendaraan');         
            $table->date('tanggal_mulai');     
            $table->date('tanggal_selesai');
            $table->string('detail');   
            $table->integer('biaya');  
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rekaps');
    }
}
