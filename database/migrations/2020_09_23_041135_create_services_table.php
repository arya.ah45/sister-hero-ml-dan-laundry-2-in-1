<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_pengajuan');        
            $table->smallInteger('monitor');        
            $table->date('tanggal_mulai');     
            $table->date('tanggal_selesai');
            $table->string('detail');   
            $table->integer('biaya');   
            $table->timestamps();

            $table->foreign('id_pengajuan')->references('id')->on('pengajuans')->onDelete('cascade')->onUpdate('cascade');    
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services');
    }
}
