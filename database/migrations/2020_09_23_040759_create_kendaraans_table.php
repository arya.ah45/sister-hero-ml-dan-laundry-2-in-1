<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKendaraansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kendaraans', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama_kendaraan',30);
            $table->unsignedSmallInteger('id_merek');
            $table->unsignedSmallInteger('id_jenis');
            $table->string('tipe',30);
            $table->string('warna',20);            
            $table->string('nomor_plat',20);            
            $table->string('nomor_sk',60);            
            $table->date('tanggal_sk');            
            $table->string('penanggungjawab',20)->nullable();            
            $table->timestamps();

            $table->foreign('id_merek')->references('id')->on('m_merek_kendaraans');            
            $table->foreign('id_jenis')->references('id')->on('m_jenis_kendaraans');                       
            $table->foreign('penanggungjawab')->references('nip')->on('pegawais')->onDelete(DB::raw('set null'))->onUpdate('cascade');                       
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kendaraans');
    }
}
