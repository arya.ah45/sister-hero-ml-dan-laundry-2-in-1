<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class dataPegawai extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    $data = [
    [
        'nip' => '1931733021',
        'nama_pegawai' => 'arya hafizh tofani',
        'alamat' => 'Dsn. Grompol, Ds. ngebrak, Kediri',
        'jabatan' => 'Ketua Kelompok',
        'bagian' => 'doggy div.',
        'no_hp' => '089666171252'
    ],
    [
        'nip' => '1931733007',
        'nama_pegawai' => 'Indah Wulansari',
        'alamat' => 'Bujel',        
        'jabatan' => 'Anggota',
        'bagian' => 'doggy div.',
        'no_hp' => '089666197630'
    ],
    [
        'nip' => '1931733001',
        'nama_pegawai' => 'Sindi Nur Kharisma',
        'alamat' => 'moro moro suko',        
        'jabatan' => 'Anggota',
        'bagian' => 'doggy div.',
        'no_hp' => '081358101509'
    ],
    [
        'nip' => '1931733117',
        'nama_pegawai' => 'Yanda Muhammad Naufall. A',
        'alamat' => 'Prambon Nganjuk',        
        'jabatan' => 'Anggota',
        'bagian' => 'doggy div.',
        'no_hp' => '082234363667'
    ]

    ];

    DB::table('pegawais')->insert($data);
    }
}
