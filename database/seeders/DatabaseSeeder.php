<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(dataJenisKendaraan::class);
        $this->call(dataMerekKendaraan::class);
        $this->call(dataPegawai::class);
        $this->call(dataUser::class);
    }
}
