<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\m_merek_kendaraan;

class dataMerekKendaraan extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = new m_merek_kendaraan;
        $admin->nama_merek = 'Suzuki';
        $admin->save();

        $admin = new m_merek_kendaraan;
        $admin->nama_merek = 'Ford';
        $admin->save();

        $admin = new m_merek_kendaraan;
        $admin->nama_merek = 'Chevrolet';
        $admin->save();

        $admin = new m_merek_kendaraan;
        $admin->nama_merek = 'Honda';
        $admin->save();
    }
}
