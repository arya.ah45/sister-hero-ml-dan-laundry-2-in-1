<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Hash;
use App\Models\User;

class dataUser extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $admin = new User;
        $admin->username = 'admin';
        $admin->password = Hash::make('admin123');
        $admin->level = 'admin';
        $admin->nip = null;
        $admin->save();
        
        
        $verifikator = new User;
        $verifikator->username = 'verifikator';
        $verifikator->password = Hash::make('verifikator123');
        $verifikator->level = 'verifikator';
        $verifikator->nip = null;
        $verifikator->save();

        $user = new User;
        $user->username = 'user';
        $user->password = Hash::make('user123');
        $user->level = 'user';
        $user->nip = null;
        $user->save();


        $kabag = new User;
        $kabag->username = 'kabag';
        $kabag->password = Hash::make('kabag123');
        $kabag->level = 'kabag';
        $kabag->nip = null;
        $kabag->save();


    }
}
