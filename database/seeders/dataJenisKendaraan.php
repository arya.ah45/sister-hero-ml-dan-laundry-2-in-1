<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\m_jenis_kendaraan;

class dataJenisKendaraan extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = new m_jenis_kendaraan;
        $admin->nama_jenis = 'minibus';
        $admin->save();

        $admin = new m_jenis_kendaraan;
        $admin->nama_jenis = 'Pick up';
        $admin->save();

        $admin = new m_jenis_kendaraan;
        $admin->nama_jenis = 'Motor';
        $admin->save();

    }
}
