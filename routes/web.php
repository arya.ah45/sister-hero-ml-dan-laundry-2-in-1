<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('hero', 'HeroController@getHero');

Route::get('paket', 'pemilikController@indexPaket');
Route::post('paket/save', 'pemilikController@simpanPaket')->name('simpanPaket');
Route::get('paket/hapus/{paketId}', 'pemilikController@hapusPaket');

Route::get('customer', 'kasirController@indexCustomer');
Route::post('customer/save', 'kasirController@simpanCustomer')->name('simpanCustomer');
Route::get('customer/hapus/{customerId}', 'kasirController@hapusCustomer');
Route::get('customer/scan', 'kasirController@scanTR');

Route::get('transaksi', 'kasirController@indexTransaksi');
Route::post('transaksi/save', 'kasirController@simpanTransaksi')->name('simpanTransaksi');
Route::get('transaksi/hapus/{transaksiId}', 'kasirController@hapusTransaksi');
Route::get('transaksi/print/{transaksiId}', 'kasirController@printTransaksi');
Route::post('transaksi/finish', 'kasirController@finishTransaksi')->name('finish_tr');

Route::get('role', 'HeroController@getRole');
Route::post('simpanRole', 'HeroController@simpanRole')->name('simpanRole');
Route::get('role/hapus/{heroId}/{roleId}', 'HeroController@hapusRole');

Route::get('skin', 'HeroController@getSkin');
Route::post('simpanSkin', 'HeroController@simpanSkin')->name('simpanSkin');
Route::get('skin/hapus/{skinId}', 'HeroController@hapusSkin');

// Route::post('login', 'Auth\LoginController@login')->name('sendlogin');

// Route::get('/home', 'HomeController@index')->name('home');



