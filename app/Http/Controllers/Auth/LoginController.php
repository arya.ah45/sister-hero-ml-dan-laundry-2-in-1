<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Models\User;
use Hash;
use Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
        
        $this->validate($request, [
            'username' => 'required',
            'password' => 'required|min:6'
        ]);
            
        $username = $request->input("username");
        $password = $request->input("password");
        $user = User::where("username", $username)->first();
        // dd($user);
        // return response()->json($user);
        
        if (!$user) {
            $out = [
                "message" => "login_failed",
                "code"    => 401,
                "result"  => [
                    "token" => null,
                ]
            ];
            return response()->json($out, $out['code']);
        }

        if (Hash::check($password, $user->password)) {
            // return 'ok';

            $dataUser = User::leftJoin('pegawais as p', 'users.nip', '=', 'p.nip')
                        ->where('users.username','=',$user->username)->get()->first();
            $data_user = [
                'nip' => $dataUser->nip,
                'level' => $dataUser->level,
                'nama_pegawai' => $dataUser->nama_pegawai,
                'jabatan' => $dataUser->jabatan,
                'bagian' => $dataUser->bagian,
                'no_hp' => $dataUser->no_hp
            ];

            // dd($dataUser);
            session::put('user_data', $data_user);

            if ($user->level == 'admin') {

                return redirect()->route('landing_admin');

            }elseif($user->level == 'verifikator'){

                return redirect('verifikator/');

            }elseif($user->level == 'user'){

                return redirect('user/');

            }elseif($user->level == 'kabag'){

                return redirect('kabag/');

            }
        } else {
            return redirect()->route('login')->with('error','Email-Address And Password Are Wrong.');
        }
    }

}
