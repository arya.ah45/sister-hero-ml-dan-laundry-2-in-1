<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class pemilikController extends Controller
{
    public function indexPaket()
    {
        $datas = DB::table('paket')->get();
        $data['dataPaket'] = $datas;     
        // return $data;
        return view('laundry.pemilik.paket', $data);

    }

    public function simpanPaket(Request $request)
    {
        $trueID = DB::select("SELECT CONCAT('pkt',
            (
                SELECT LPAD(
                    (
                    SELECT IFNULL((SELECT SUBSTR(PKTID,5) FROM paket ORDER BY LPAD(SUBSTR(PKTID,5), 4, '0') DESC LIMIT 1),0) + 1 AS id
                    ),4,'0'
                )
            )
        ) AS 'id';");
        
        $data_array = array(
            'PKTNAMA' => $request->nama,
            'PKTHARGA' => $request->harga,
            'PKTID' =>  $trueID[0]->id         
        );

        if($request->action == "tambah"){
            DB::table('paket')->insert($data_array); 
        }else{
            $data_array['PKTID'] = $request->id;
            DB::table('paket')->where('PKTID','=',$request->id)->update($data_array); 
        }

        $data['message']="Sukses Tambah Paket !";
 
        return response()->json($data);        
    }

    public function hapusPaket($paketId)
    {
        $query = DB::table('paket')
            ->where([
            ['PKTID', '=', $paketId]
            ])->delete();  

        if ( $query == true ) {
            $data['code']="100";
            $data['message']="Sukses hapus data paket!";
        } else {
            $data['code']="404";
            $data['message']="Gagal hapus data paket!";
        }        
        return response()->json($data);
    }
}
