<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class heroController extends Controller
{
    
    public function getHero()
    {
        $data = DB::table('hero as h')
        ->get();

        //             $skins = DB::select('select * from hero h, skin s
        //                         where h.heroid = s.heroid '); 
            

        // return $skins;

        foreach ($data as $key => $values) {
            $hero[$key]['heroid'] = $values->HEROID;
            $hero[$key]['heronama'] = $values->HERONAMA;

            $roles = DB::select('select * from punyarole pr, role r
                                where pr.roleid = r.roleid 
                                and pr.heroid = ?', [$values->HEROID]); 
            
            $keys=0;
            foreach ($roles as $keys => $value) {
                $role[$keys] = $value->ROLENAMA;
            }

            $hero[$key]['role'] = $role;

            $skins = DB::select('select * from hero h, skin s
                                where h.heroid = s.heroid 
                                and h.heroid = ?', [$hero[$key]['heroid']]); 

            $hero[$key]['skin'] = $skins;
        }
        // return $hero;
        $data['dataHero'] = $hero;
        return view('hero.home', $data);

    }

    public function getRole()
    {
        $roles = DB::select('select * from hero h, punyarole pr, role r
                            where h.heroid = pr.heroid
                            and pr.roleid = r.roleid'); 
            //   return $roles;   
        $data['optHero'] = DB::table('hero')->orderBy('heronama', 'asc')->get();
        $data['optRole'] = DB::table('role')->orderBy('rolenama', 'asc')->get();
        $data['dataRole'] = $roles;               
        return view('hero.role',$data);
    }

    public function getSkin()
    {
        $skin = DB::select('select * from hero h, skin s
                            where h.heroid = s.heroid'); 
        // return $skin;
        $last = DB::select('select max(skinid) as lastId from skin');               
        $data['lastId'] = $last[0]->lastId;

        $data['dataSkin']= $skin;               
        $data['optHero'] = DB::table('hero')->orderBy('heronama', 'asc')->get();

        return view('hero.skin',$data);
    }

    public function simpanRole(Request $request)
    {
        $dataRole = [
            'HEROID' => $request->hero_id,
            'ROLEID' => $request->role_id,
        ];
        DB::table('punyarole')->insert($dataRole); 

        $data['message']="Sukses Tambah Role !";
 
        return response()->json($data);
    }

    public function hapusRole($heroId,$roleId)
    {
        $query = DB::table('punyarole')
            ->where([
            ['HEROID', '=', $heroId],
            ['ROLEID', '=', $roleId]
            ])->delete();  

        if ( $query == true ) {
            $data['code']="100";
            $data['message']="Sukses hapus data pasien!";
        } else {
            $data['code']="404";
            $data['message']="Gagal hapus data pasien!";
        }        
        return response()->json($data);

    }

    public function simpanSkin(Request $request)
    {
        $dataSkin = [
            'HEROID' => $request->hero_id,
            'SKINID' => $request->skin_id,
            'SKINNAMA' => $request->skin_nama,
            'SKINHARGA' => $request->skin_harga,
        ];
        DB::table('skin')->insert($dataSkin); 

        $data['message']="Sukses Tambah Role !";
 
        return response()->json($data);
    }

    public function hapusSkin($skinId)
    {
        $query = DB::table('skin')
            ->where([
            ['SKINID', '=', $skinId]
            ])->delete();  

        if ( $query == true ) {
            $data['code']="100";
            $data['message']="Sukses hapus data pasien!";
        } else {
            $data['code']="404";
            $data['message']="Gagal hapus data pasien!";
        }        
        return response()->json($data);

    }
}
