<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
USE PDF;
USE DeHelper;

class kasirController extends Controller
{
    // ======================== ------------------------------- Customer ----------------------------------- ========================
    public function indexCustomer()
    {
        $datas = DB::table('customer')->get();
        $data['dataCustomer'] = $datas;     
        // return $data;
        return view('laundry.kasir.customer', $data);

    }

    public function simpanCustomer(Request $request)
    {
        $trueID = DB::select("SELECT CONCAT('cust',
            (
                SELECT LPAD(
                    (
                    SELECT IFNULL((SELECT SUBSTR(CUSTID,5) FROM customer ORDER BY LPAD(SUBSTR(CUSTID,5), 4, '0') DESC LIMIT 1),0) + 1 AS id
                    ),4,'0'
                )
            )
        ) AS 'id';");
        
        $data_array = array(
            'CUSTNAMA' => $request->nama,
            'CUSTHP' => $request->hp,
            'CUSTID' =>  $trueID[0]->id         
        );

        if($request->action == "tambah"){
            DB::table('customer')->insert($data_array); 
        }else{
            $data_array['CUSTID'] = $request->id;
            DB::table('customer')->where('CUSTID','=',$request->id)->update($data_array); 
        }

        $data['message']="Sukses Tambah Customer !";
 
        return response()->json($data);        
    }

    public function hapusCustomer($paketId)
    {
        $query = DB::table('customer')
            ->where([
            ['CUSTID', '=', $paketId]
            ])->delete();  

        if ( $query == true ) {
            $data['code']="100";
            $data['message']="Sukses hapus data paket!";
        } else {
            $data['code']="404";
            $data['message']="Gagal hapus data paket!";
        }        
        return response()->json($data);
    }

    // ======================== ------------------------------- Transaksi ----------------------------------- ========================
    public function indexTransaksi()
    {
        $datas = DB::table('transaksi as tr')
                    ->leftJoin('paket as p','p.PKTID','=','tr.PKTID')
                    ->leftJoin('customer as c','c.CUSTID','=','tr.CUSTID')
                    ->selectRaw("tr.*, CUSTNAMA, CUSTHP, PKTNAMA, PKTHARGA")
                    ->get();
        $optPaket = DB::table('paket')->get();
        $optCust = DB::table('customer')->get();
        $data['dataTransaksi'] = $datas;
        $data['optPaket'] = $optPaket;
        $data['optCust'] = $optCust;
        
        // return $data;
        return view('laundry.kasir.transaksi', $data);
    }

    public function simpanTransaksi(Request $request)
    {

        
        $data_array = array(
            'CUSTID' => $request->cust_id,
            'TRTGL' => $request->tanggal,
            'PKTID' => $request->paket_id,
            'TRBERAT' => $request->trberat,
            'TRTOTAL' => $request->trtotal         
        );

        if($request->action == "tambah"){
            DB::table('transaksi')->insert($data_array); 
        }else{
            $data_array['TRID'] = $request->id;
            DB::table('transaksi')->where('TRID','=',$request->id)->update($data_array); 
        }

        $data['message']="Sukses Tambah Transaksi !";
 
        return response()->json($data);        
    }

    // public function hapusTransaksi($paketId)
    // {
    //     $query = DB::table('customer')
    //         ->where([
    //         ['CUSTID', '=', $paketId]
    //         ])->delete();  

    //     if ( $query == true ) {
    //         $data['code']="100";
    //         $data['message']="Sukses hapus data paket!";
    //     } else {
    //         $data['code']="404";
    //         $data['message']="Gagal hapus data paket!";
    //     }        
    //     return response()->json($data);
    // }

    public function printTransaksi($transaksiId)
    {
        $dataTR = DB::table('transaksi as t')
                    ->leftJoin('customer as c','c.CUSTID','=','t.CUSTID')
                    ->leftJoin('paket as p','p.PKTID','=','t.PKTID')
                    ->where('TRID','=',$transaksiId)->get();
        $kirim['id'] = $dataTR[0]->TRID;
        $kirim['tgl'] = $dataTR[0]->TRTGL;
        $kirim['nama'] = $dataTR[0]->CUSTNAMA;
        $kirim['data'] = $dataTR[0]->TRID
                        .'||'.$dataTR[0]->CUSTNAMA//1
                        .'||'.$dataTR[0]->CUSTHP//2
                        .'||'.$dataTR[0]->PKTNAMA//3
                        .'||'.$dataTR[0]->TRBERAT//4
                        .' kg||Rp.'.DeHelper::formatUang($dataTR[0]->TRTOTAL)//5
                        .',-||'.DeHelper::format_tanggal($dataTR[0]->TRTGL);//6

    	$pdf = PDF::loadview('laundry.kasir.nota',$kirim)->setPaper('a4', 'portrait');;
        return view('laundry.kasir.nota',$kirim);
        return $pdf->download('nota'.$dataTR[0]->TRID.'.pdf');
                
    }

    public function scanTR()
    {
        return view('laundry.kasir.scanTR');
    }

    public function finishTransaksi(Request $request)
    {
        // return $transaksiId;
        DB::table('transaksi')->where([['TRID', '=', $request->id]])->update(['TRSTATUS' => 'Selesai']);
        return response()->json("Transaction Finished !");
        try {
        } catch (\Throwable $th) {
            //throw $th;
        }
    }
}
