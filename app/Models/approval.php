<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class approval extends Model
{
    // use HasFactory;
    protected $table = 'approvals';
    protected $primaryKey ='id';
  
      protected $fillable = [
        'id_pengajuan',
        'status',
        'catatan',
        'created_by'
    ];
}
