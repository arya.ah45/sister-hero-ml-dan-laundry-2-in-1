<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class rekap extends Model
{
    // use HasFactory;

    protected $table = 'rekaps';
    protected $primaryKey ='id';
  
      protected $fillable = [
        'tanggal_pengajuan',
        'nama_pegawai',
        'keluhan',
        'kendaraan',
        'tanggal_mulai',
        'tanggal_selesai',
        'detail','biaya'
    ];
}
