<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class m_merek_kendaraan extends Model
{
    // use HasFactory;

    protected $table = 'm_merek_kendaraans';
    protected $primaryKey ='id';
  
      protected $fillable = [
        'nama_merek'
    ];
}
