<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class kendaraan extends Model
{
    // use HasFactory;

    protected $table = 'kendaraans';
    protected $primaryKey ='id';
  
      protected $fillable = [
        'nama_kendaraan',
        'id_merek',
        'id_jenis',
        'tipe',
        'warna',
        'nomor_plat',
        'nomor_sk',
        'tanggal_sk',
        'penanggungjawab'
    ];
}
