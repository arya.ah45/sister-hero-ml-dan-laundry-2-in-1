<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class pegawai extends Model
{
    // use HasFactory;

    protected $table = 'pegawais';
    protected $primaryKey ='nip';
  
      protected $fillable = [
        'nama_pegawai',
        'alamat',
        'jabatan',
        'bagian',
        'no_hp'
    ];
}
