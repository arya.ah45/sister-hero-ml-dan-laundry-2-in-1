<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class service extends Model
{
    // use HasFactory;

    protected $table = 'services';
    protected $primaryKey ='id';
  
      protected $fillable = [
        'id_pengajuan',
        'monitor',
        'tanggal_mulai',
        'tanggal_selesai',
        'detail',
        'biaya'
    ];
}
