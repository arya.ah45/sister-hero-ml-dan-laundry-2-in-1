<?php
namespace App\Helpers;

use Illuminate\Http\Request;
use Session;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;


class DeHelper
{

    public static function format_tanggal($tgl)
    {

        $bln = date("m", strtotime($tgl));
        $bln_h = array('01' => "Januari", "02" => "Februari", "03" => "Maret", "04" => "April", "05" => "Mei", "06" => "Juni", "07" => "Juli", "08" => "Agustus", "09" => "September", "10" => "Oktober", "11" => "Nopember", "12" => "Desember");
        $bln = $bln_h[$bln];
        $tg = date("d", strtotime($tgl));
        $thn = date("Y", strtotime($tgl));
        $print = $tg.' '.$bln.' '.$thn;

        return $print;
    }

    public static function sideMenu($level)
    {
            $validasi = (request()->is('*admin*')) ? 'active' : '';
            $master = (request()->is('*master/*')) ? 'active' : '';
            $kendaraan = (request()->is('*kendaraan*')) ? 'active' : '';
            $personalia = (request()->is('*personalia/*')) ? 'active' : '';
            $backup = (request()->is('*backup*')) ? 'active' : '';
            $service = (request()->is('*service*')) ? 'active' : '';

            $rule = [
                'admin' => [0,1,2,3,4],
                'kabag' => [1,2,3,4],
                'verifikator' => [5]
            ];

            $sideMenu = array(
                '0' =>'<li class="nav-item '.$validasi.' ">
                            <a href="#">
                                <i class="fas phpdebugbar-fa-check-circle"></i>
                                <p>Validasi</p>
                            </a>
                        </li> ',
                '1' =>'<li class="nav-item '.$master.'">
                            <a data-toggle="collapse" href="#master">
                                <i class="fas fa-database"></i>
                                <p>Master</p>
                                <span class="caret"></span>
                            </a>
                            <div class="collapse" id="master">
                                <ul class="nav nav-collapse">
                                    <li>
                                    <a href="#">
                                            <i class="fas fa-trademark"></i>
                                            <p>Merek Kendaraan</p>
                                        </a>
                                    </li>
                                    <li>
                                    <a href="#">
                                            <i class="fas fa-gas-pump"></i>
                                            <p>Jenis Kendaraan</p>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>',
                '2' =>'<li class="nav-item '.$kendaraan.'">
                            <a href="#">
                                <i class="fas fa-car-side"></i>
                                <p>Kendaraan</p>
                            </a>
                        </li>',
                '3' =>'<li class="nav-item '.$personalia.' ">
                            <a data-toggle="collapse" href="#user">
                                <i class="fas fa-user"></i>
                                <p>Kelola User</p>
                                <span class="caret"></span>
                            </a>
                            <div class="collapse" id="user">
                                <ul class="nav nav-collapse">
                                    <li>
                                    <a href="#">
                                            <i class="fas fa-user-tie"></i>
                                            <p>Pegawai</p>
                                        </a>
                                    </li>
                                    <li>
                                    <a href="#">
                                            <i class="fas fa-user-edit"></i>
                                            <p>Akun</p>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>',
                '4' =>'<li class="nav-item '.$backup.'">
                            <a href="#">
                                <i class="fas fa-file-archive"></i>
                                <p>Backup</p>
                            </a>
                        </li>',
                '5' =>'<li class="nav-item '.$service.'">
                            <a href="#">
                                <i class="fas fa-wrench"></i>
                                <p>Service</p>
                            </a>
                        </li>'
            );

            foreach ($rule[$level] as $key => $value) {
                $menu[$key] = $sideMenu[$value];
            }
            return $menu;
    }

    public static function status($ket)
    {
        $status = array(
            '0' =>'<span class="badge badge-secondary">Diperbaiki</span>',
            '1' =>'<span class="badge badge-secondary">Draf</span>',
            '2' =>'<span class="badge badge-primary">Disetujui Admin</span>',
            '3' =>'<span class="badge badge-danger">Ditolak Admin</span>',
            '4' =>'<span class="badge badge-success">Mengantri</span>',
            '5' =>'<span class="badge badge-danger">Diperbaiki</span>',
            '6' =>'<span class="badge badge-warning">Selesai</span>'
        );
        if (is_numeric($ket)) {
            if (isset($status[$ket])) {
                $data = $status[$ket];
            } else {
                $data = $status[$ket];
            }
        }else{
            $data = $status;
        }
        return $data;
    }
    
    public static function singkatNama($str)
    {
        $namaDepan = array_slice(explode(' ', $str), 0, 1);

        $totalNama = count(explode(' ', $str)); 

        $namaBelakang = array_slice(explode(' ', $str), -($totalNama)+1);

        foreach ($namaBelakang as $key => $value) {
            $nama[$key] = strtoupper(substr($namaBelakang[$key],0,1)).'. ';
        }
        
        return implode(' ',array_merge($namaDepan,$nama));
    }

    public static function formatUang($var)
    {
        if(is_numeric($var)){

            if(floor($var) != $var){
                $var = number_format($var, 2, ',', '.');
            }else{
                $var = number_format($var, 0, ',', '.');
            }

        }else{
            $var = 0;
        }

        return $var;   
    }
}
