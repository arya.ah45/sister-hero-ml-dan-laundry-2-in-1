<!DOCTYPE html>
<html lang="en">

<head>
    @include('layout_user.header')
</head>

<body>
    <!-- Main Content -->
    <!-- Content -->
    @yield('content')
</body>

@include('layout_user.script')

</html>