<nav class="navbar navbar-transparent navbar-color-on-scroll fixed-top navbar-expand-lg" color-on-scroll="100"
    id="sectionsNav">
    <div class="container">
        <div class="navbar-translate">
            <a class="navbar-brand py-0 my-0" href="#">
                <img src="{{asset('asset_user/img/logoPakar.png')}}" width="200PX" id="gbPutih">
                <img src="{{asset('asset_user/img/logoPakarAdmin.png')}}" width="200PX" id="gbHitam" style="display:none">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" aria-expanded="false"
                aria-label="Toggle navigation">
                <span class="sr-only">Toggle navigation</span>
                <span class="navbar-toggler-icon"></span>
                <span class="navbar-toggler-icon"></span>
                <span class="navbar-toggler-icon"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="{{url('/user')}}">
                    <i class="material-icons">build</i>Status Service
                    </a>
                </li>
                <li class="nav-item">
                <a class="nav-link" href="{{url('/user/detail_kendaraan')}}">
                    <i class="material-icons">directions_car</i> Detil Kendaraan
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{url('/user/about')}}">
                    <i class="material-icons">phone</i> About
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>
