<!DOCTYPE html>
<html lang="en">

<head>    
    @include('layout_user.header')

<style>
  .form-check .form-check-input:checked~.form-check-sign .check {
    background: #205ccc;
  }

 .form-controls {
    display: block;
    width: 100%;
    padding: 5px;
    font-size: 1rem;
    line-height: 1.5;
    color: #495057;
    background-color: rgba(0, 0, 0, 0);
    background-clip: padding-box;
    border: 1px solid #d2d2d2;
    border-radius: 3;
    box-shadow: none;
    transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
  }

  .form-controls::-ms-expand {
      background-color: transparent;
      border: 0;
  }
  
  .form-controls:focus {
      color: #495057;
      background-color: rgba(0, 0, 0, 0);
      border-color: #4a60df;
      outline: 0;
      box-shadow: none, 0 0 0 0.2rem rgba(34, 19, 245, 0.25);
  }

  .profile-page .profile img {
      max-width: 230px;
      width: 100%;
      margin: 0 auto;
      -webkit-transform: translate3d(0, -50%, 0);
      -moz-transform: translate3d(0, -50%, 0);
      -o-transform: translate3d(0, -50%, 0);
      -ms-transform: translate3d(0, -50%, 0);
      transform: translate3d(0, -50%, 0);
  }
    
  .container1 {
    position: relative;
    width: 100%;
    height: 50%
  }

  .image1 {
    display: block;
    width: 100%;
    height: auto;
  }

  .overlay1 {
    position: absolute;
    bottom: 0;
    left: 100%;
    right: 0;
    background-color: #47a44b;
    overflow: hidden;
    width: 0;
    height: 100%;
    transition: .5s ease;
  }

  .container1:hover .overlay1 {
    width: 100%;
    left: 0;
    opacity:0.8
  }

  .text1 {
    color: white;
    font-size: 20px;
    position: absolute;
    top: 50%;
    left: 50%;
    -webkit-transform: translate(-50%, -50%);
    -ms-transform: translate(-50%, -50%);
    transform: translate(-50%, -50%);
  }

  .text2 {
    color: white;
    font-size: 20px;
    position: absolute;
    top: 90%;
    left: 30%;
    -webkit-transform: translate(-40%, -50%);
    -ms-transform: translate(-40%, -50%);
    transform: translate(-40%, -50%);
  }

  /* actual styles */
  .slick-prev:before,
  .slick-next:before {
  color: black;
  font-size: 30px;  
  }

  .slick-list {
      position: relative;
      display: block;
      overflow: hidden;
      margin: 15px;
      padding: 10px;
  }

    /* Style the tab */
  .tab {
    overflow: hidden;
    border: 1px solid #ccc;
    background-color: #f1f1f1;
  }

  /* Style the buttons inside the tab */
  .tab button {
    background-color: inherit;
    float: left;
    border: none;
    outline: none;
    cursor: pointer;
    padding: 2% 1%;
    transition: 0.3s;
    font-size: 17px;
    width: 25%;
  }

  /* Change background color of buttons on hover */
  .tab button:hover {
    background-color: #eee;
  }

  /* Create an active/current tablink class */
  .tab button.active {
    background-color: #ccc;
  }

  /* Style the tab content */
  .tabcontent {
    display: none;
    padding: 6px 12px;
    border: 1px solid #ccc;
    border-top: none;
  }

</style>
</head>

@if(request()->is('*/detail_kendaraan'))
<body class="profile-page sidebar-collapse">
    @include('layout_user.navbar')
    <!-- Main Content -->
    <!-- Content -->
    @yield('content')
</body>
@else

<body>
    @include('layout_user.navbar')
    <!-- Main Content -->
    <!-- Content -->
    @yield('content')
</body>
@endif

@if(request()->is('*/about'))
@else
@include('layout_user.footer')
@endif

@include('layout_user.script')
  
@stack('scripts')
</html>