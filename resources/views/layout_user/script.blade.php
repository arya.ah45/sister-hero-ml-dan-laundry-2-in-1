<!--   Core JS Files   -->
{{-- <script src="{{asset('asset_user/js/core/jquery.min.js')}}" type="text/javascript"></script> --}}
<script src="{{asset('asset_user/js/core/popper.min.js')}}" type="text/javascript"></script>
<script src="{{asset('asset_user/js/core/bootstrap-material-design.min.js')}}" type="text/javascript"></script>
<script src="{{asset('asset_user/js/plugins/moment.min.js')}}"></script>
<!--	Plugin for the Datepicker, full documentation here: https://github.com/Eonasdan/bootstrap-datetimepicker -->
<script src="{{asset('asset_user/js/plugins/bootstrap-datetimepicker.js')}}" type="text/javascript"></script>
<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="{{asset('asset_user/js/plugins/nouislider.min.js')}}" type="text/javascript"></script>
<!--  Google Maps Plugin    -->
<!-- Control Center for Material Kit: parallax effects, scripts for the example pages etc -->
<script src="{{asset('asset_user/js/material-kit.js?v=2.0.7')}}" type="text/javascript"></script>
<script src="{{asset('js/toastr.js')}}" type="text/javascript"></script>

<!-- Sweet Alert -->
<script src="{{asset('assets/js/plugin/sweetalert/sweetalert.min.js')}}"></script>

{{-- 
<script src="{{asset('asset_particle/particles.js')}}"></script>
<script src="{{asset('asset_particle/js/app.js')}}"></script> --}}

{{-- <script type="text/javascript" src="{{asset('slick/slick.min.js')}}"></script> --}}

{{-- <script src="{{asset('colorlib-wizard-23/js/main.js')}}"></script>
<script src="{{asset('colorlib-wizard-23/js/jquery.steps.js')}}"></script>
<script src="{{asset('colorlib-wizard-23/js/jquery-ui.min.js')}}"></script> --}}