<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Sistem Pakar</title>
    <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
    <link rel="icon" href="{{asset('asset_user/img/pakar.ico')}}" type="image/x-icon" />
    <link rel="stylesheet" href="{{asset('asset_particle/css/style.css')}}">
    
    <!-- CSS Files -->
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/atlantis.min.css')}}">
    
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link rel="stylesheet" href="{{asset('assets/css/demo.css')}}">
    
    <!-- Fonts and icons -->
    <script src="{{asset('assets/js/plugin/webfont/webfont.min.js')}}"></script>
    {{-- <script src = "https://code.highcharts.com/highcharts.js" ></script>
    <script src = "https://code.highcharts.com/modules/exporting.js"> </script> 
    <script src = "https://code.highcharts.com/modules/export-data.js" > </script>
    <script src = "https://code.highcharts.com/modules/accessibility.js" > </script> --}}
    <script>
        WebFont.load({
            google: {
                "families": ["Lato:300,400,700,900"]
            },
            custom: {
                "families": ["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands",
                    "simple-line-icons"
                ],
                urls: ["{{asset('assets/css/fonts.min.css')}}"]
            },
            active: function() {
                sessionStorage.fonts = true;
            }
        });
    </script>    

    <script src="{{asset('js/jquery-3.5.1.js')}}"></script>
    <script src="{{asset('js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('js/dataTables.bootstrap4.min.js')}}"></script>
    {{-- <script src="{{asset('js/jparticle.jquery.min.js')}}"></script> --}}
    <script src="{{asset('js/jquery.validate.js')}}"></script>

    {{-- particle --}}
 


</head>

<body>
                        <table class="table">
                            <tr>
                                <td colspan="2"><h2>NOTA TRANSAKSI</h2></td>
                            </tr>
                            <tr>
                                <td width="20%">
                                    <img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->size(250)->generate($data)) !!} ">
                                </td>
                            </tr>
                                    <tr>
                                        <td><h6 class="mb-0">ID Transaksi</h6></td>
                                    </tr>
                                    <tr>
                                        <td><h3 class="mt-0 mb-0">{{$id}}</h3></td>
                                    </tr>
                                    <tr>
                                        <td><h6 class="mb-0">Tanggal Transaksi</h6></td>
                                    </tr>
                                    <tr>
                                        <td><h3 class="mt-0 mb-0">{{Str::substr($tgl, 0, 10)}}</h3></td>
                                    </tr>
                                    <tr>
                                        <td><h6 class="mb-0">Nama Customer</h6></td>
                                    </tr>
                                    <tr>
                                        <td><h3 class="mt-0 mb-0">{{$nama}}</h3></td>
                                    </tr>

                        </table>
</body>

@include('layouts.script')

@stack('scripts')
</html>
