@extends('layouts.main')

@section('content')

<div class="panel-header bg-secondary-gradient">
    <div class="page-inner py-5">
        <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
            <div class="col-12 text-center">
                <h2 class="text-white pb-2 fw-bold">Wellcome to Laundry's</h2>               
            </div>
        </div>
    </div>
</div>

<div class="page-inner mt--5 pb-0 w-100" style="position: absolute;">
    <div class="row mt--2">
        <div class="col-md-12">    
            <div class="card">
                {{-- atas --}}
                <div class="card-header">
                    <div class="card-head-row card-tools-still-right">
                        <h4 class="card-title">Data Transaksi </h4>
                        <div class="card-tools">
                                <div class="span-mode">
                                    <div class="pull-left mr-2">
                                        <button class="btn btn-primary btn-xs btn-round ml-auto btnTambah float-left" data-toggle="modal" data-target="#modalpenyakit">
                                            <i class="fa fa-plus"></i>&nbsp Transaksi
                                        </button>
                                    </div>
                                    <div class="pull-left mr-2">
                                        <button class="btn btn-info btn-xs btn-round ml-auto scanner float-left">
                                            <i class="fa fa-qrcode"></i>&nbsp Scan QR
                                        </button>                        
                                    </div>
                                </div>                            
                        </div>
                    </div>
                </div>
    
                {{-- isi table --}}
                <div class="card-body scroll">

                    <div class="table-responsive">
                        <table id="table_gejala" class="display table table-striped table-hover" >
                            <thead>
                                <tr>
                                    <th width="5%">No</th>
                                    <th width="15%">Customer / No HP</th>
                                    <th width="10%">Paket</th>
                                    <th width="15%">Tanggal</th>
                                    <th width="10%">Berat</th>
                                    <th width="10%">Total</th>
                                    <th width="10%">Status</th>
                                    <th width="10%" align="center">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($dataTransaksi as $keys => $item)
                                <tr>
                                    <td>{{$keys+1}}</td>
                                    <td>{{$item->CUSTNAMA.' / '.$item->CUSTHP}}</td>
                                    <td>{{$item->PKTNAMA." @Rp.".DeHelper::formatUang($item->PKTHARGA).',-'}}</td>       
                                    <td>{{DeHelper::format_tanggal($item->TRTGL)}}</td>
                                    <td>{{$item->TRBERAT}} kg</td>
                                    <td>Rp.{{DeHelper::formatUang($item->TRTOTAL)}},-</td>
                                    <td>{{$item->TRSTATUS}}</td>
                                    <td>
                                        {{-- <a href="javascript:void(0)" data-trid="{{$item->TRID}}" class="btn btn-danger btn-xs btn-block mt-2 mb-0 deleteData">HAPUS</a>  --}}
                                        <a href="{{url()->current().'/print/'.$item->TRID}}" data-trid="{{$item->TRID}}" class="btn btn-success btn-xs btn-block printTR">PRINT QR-Code</a> 
                                        {{-- <a href="javascript:void(0)" data-trid="{{$item->TRID}}" data-custid="{{$item->CUSTID}}" data-trtgl="{{$item->TRTGL}}" data-pktid="{{$item->PKTID}}" data-trberat="{{$item->TRBERAT}}" data-trtotal="{{$item->TRTOTAL}}" class="btn btn-warning btn-xs btn-block mb-2 mt-0 btnEdit">EDIT</a>  --}}
                                    </td>       
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modalpenyakit" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header no-bd">
                <h5 class="modal-title">
                    <span class="fw-mediumbold" id="title"></span>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="formTR"  name="formTR">
                    @csrf
                    <input id="action" hidden type="text" name="action" class="form-control" value="">
                    <input id="id" hidden type="text" name="id" class="form-control" value="">
                    
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group form-group-default">
                                <label>Pilih Customer</label>
                                <select class="form-control" name="cust_id" id="cust_id">
                                    <option value="0" data-harga="0" selected disabled>pilih customer</option>
                                    @foreach ($optCust as $item)
                                    <option value="{{$item->CUSTID}}">{{$item->CUSTNAMA}}</option>
                                    @endforeach                                    
                                </select>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group form-group-default" id="cekharga">
                                <label>Pilih Paket Laundry</label>
                                <select class="form-control" name="paket_id" id="paket_id">
                                    <option value="0" data-harga="0" selected disabled>pilih paket</option>
                                    @foreach ($optPaket as $item)
                                    <option value="{{$item->PKTID}}" data-harga="{{$item->PKTHARGA}}">{{$item->PKTNAMA}}</option>
                                    @endforeach                                    
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group form-group-default">
                                <label>Tanggal</label>
                                <input id="tanggal" type="date" name="tanggal" class="form-control" value="">
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group form-group-default">
                                <label>Berat Cucian</label>
                                <input id="trberat" type="number" name="trberat" class="form-control" value="">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group form-group-default">
                                <label>Total Biaya Cucian</label>
                                <input id="trtotal" readonly type="text" name="trtotal" class="form-control" value="">
                            </div>
                        </div>
                    </div>
            </div>
                <div class="modal-footer no-bd">
                    <button type="reset" class="btn btn-danger" data-dismiss="modal">Tutup</button>
                    <button type="button" id="btnSave" class="btn btn-primary">Tambah</button>
                </div>
            </form>
            
        </div>
    </div>
</div>

<div class="modal modal-fade-in-scale-up bs-example-modal-lg show" id="modal_qr" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-center">
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title">Scan</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <div class="form-group row">
                    <div class="col-md-12" id="div_select" style="display: none">
                        <div class="form-group">
                            <label class="control-label">Kamera</label>
                            <select class="form-control select" id="popup_camera">

                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-12" id="">
                        <video id="preview"></video>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default btn_batal_scan" data-dismiss="modal">Batal</button>                
            </div>

        </div>
    </div>
</div>

<div class="modal modal-fade-in-scale-up bs-example-modal-lg show" id="dtl_transaksi" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-center">
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title">Detail Transaksi</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <h2>Customer</h2>
                        <p id="customer"></p>
                    </div>
                    <div class="col-md-6">
                        <h2>No. HP</h2>
                        <p id="no_hp"></p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <h2>Paket</h2>
                        <p id="paket"></p>
                    </div>
                    <div class="col-md-6">
                        <h2>Tanggal</h2>
                        <p id="tgl"></p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <h2>Berat</h2>
                        <p id="berat"></p>
                    </div>
                    <div class="col-md-6">
                        <h2>Total</h2>
                        <p id="total"></p>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <a href="javascript:void(0)" type="button" class="btn btn-success" id="link">SELESAI</a>                
            </div>

        </div>
    </div>
</div>

<script src="https://rawgit.com/schmich/instascan-builds/master/instascan.min.js"></script>
<script>


    var scanner = new Instascan.Scanner({
        video: document.getElementById('preview'),
        scanPeriod: 5,
        mirror: false
    });

    function animasi_leave(this_) {
        $(this_).css({
            "border": "solid",
            "border-color": "#3e8ef7"
        });
    }

    function animasi_enter(this_) {
        $(this_).css({
            "border": "",
            "border-color": ""
        });
    }

    $(".scanner").click(function() {
        scannerInstanscan();
    })

    // FUNCTION SCANNER    
    function scannerInstanscan() {
        Instascan.Camera.getCameras().then(function(cameras) {
            if (cameras.length > 0) {
                if (cameras.length > 1) {
                    $("#div_select").show();
                    $("#popup_camera").html("");
                    $("#popup_camera").append("<option value=''> -- Pilih Kamera -- </option>");
                    for (i in cameras) {
                        $("#popup_camera").append("<option value='" + [i] + "'>" + cameras[i].name + "</option>");
                    }

                    if (cameras[1] != "") {
                        scanner.start(cameras[1]);
                        $("#popup_camera").val(1).change();
                    } else {
                        alert('Kamera tidak ditemukan!');
                    }
                } else {
                    if (cameras[0] != "") {
                        scanner.start(cameras[0]);
                        $("#popup_camera").val(0).change();
                    } else {
                        alert('Kamera tidak ditemukan!');
                    }
                }

                $("#popup_camera").change(function() {
                    var id = $("#popup_camera :selected").val();

                    if (id != '') {
                        if (cameras[id] != "") {
                            scanner.start(cameras[id]);
                        } else {
                            alert('Kamera tidak ditemukan!');
                        }
                    }
                })

            } else {
                console.error('No cameras found.');
                alert('No cameras found.');
            }
        }).catch(function(e) {
            console.error(e);
            alert(e);
        });
        $("#modal_qr").modal("show");
    }
    
    scanner.addListener('scan', function(content) {
        if (content != '') {
            $("#modal_qr").modal("hide");
            var item = content.split("||");
            $('#customer').text(item[1]);
            $('#no_hp').text(item[2]);
            $('#paket').text(item[3]);
            $('#tgl').text(item[6]);
            $('#berat').text(item[4]);
            $('#total').text(item[5]);
            $('#link').attr("data-id",item[0]);
            $("#dtl_transaksi").modal("show");
            
            console.log(item);
            // window.location.href = item;
        } else {
            var item = content.split("||");
            // window.location.href = "/" + item;
            console.log(item);
            setTimeout(function() {
                swal({
                    title: "Data Error!",
                    text: "Content dari Kode QR tersebut Kosong!",
                    type: "error"
                }, function() {
                    $("#modal_qr").modal("hide");
                }, 1000);
            })
        }        
    });

    $('#link').click(function (e) {
        e.preventDefault();
        var datas = {
            id:$(this).data('id'),
            "_token": "{{ csrf_token() }}",
        };

        $.ajax({
            data: datas,
            url: "{{route('finish_tr')}}",
            type: "POST",
            dataType: 'json',
            success: function (data) {
                swal("Good job!", data, "success");
                $('#dtl_transaksi').modal('hide');
                location.reload(true);  
            },
            error: function (data) {
                console.log('Error:', data);
s            }
        });

    });

    $('#modal_qr').on('hidden.bs.modal', function () {
        scanner.stop();
    })

    $('#dtl_transaksi').on('hidden.bs.modal', function () {
            $('#customer').text('');
            $('#no_hp').text('');
            $('#paket').text('');
            $('#tgl').text('');
            $('#berat').text('');
            $('#total').text('');
    })

    //--------------------------------------------------------------------------------------------- 
    function hitungTotal(harga,berat) {
        var total = harga * berat;
        console.log(berat);
        $('#trtotal').val(total);
    }

    $('#paket_id').change(function (e) { 

        console.log($( "#paket_id option:selected" ).data('harga'));
        var harga = $( "#paket_id option:selected" ).data('harga');
        var berat = ($('#trberat').val()!='')?$('#trberat').val():1;

        hitungTotal(harga,berat)
    });

    $('#trberat').change(function (e) { 

        var harga = $( "#paket_id option:selected" ).data('harga');
        var berat = ($('#trberat').val()!='')?$('#trberat').val():1;

        hitungTotal(harga,berat)
    });



    $('.btnTambah').click(function() {
        $('#title').html('Tambah Transaksi');
        $('#action').val('tambah');
        $('#modalpenyakit').modal({
            backdrop: 'static',
            keyboard: false, // to prevent closing with Esc button (if you want this too)
            show: true
        })
    });

    $('.btnEdit').click(function() {
        $('#title').html('Edit Paket');
        $('#btnSave').html('Update');
        $('#action').val('edit');
        $('#id').val($(this).data('trid'))
        $('#nama').val($(this).data('custid'))
        $('#hp').val($(this).data('trtgl'))

        $('#modalpenyakit').modal({
            backdrop: 'static',
            keyboard: false, // to prevent closing with Esc button (if you want this too)
            show: true
        })
    });

    $('#btnSave').click(function (e) {
        e.preventDefault();
        $(this).html('Sending..');
        $.ajax({
            data: $('#formTR').serialize(),
            url: "{{route('simpanTransaksi')}}",
            type: "POST",
            dataType: 'json',
            success: function (data) {
                $('#formTR').trigger('reset');
                $('#btnSave').html('Simpan');
                $('#modalpenyakit').modal('hide');
                location.reload(true);  
            },
            error: function (data) {
                console.log('Error:', data);
                $('#btnSave').html('Simpan');
            }
        });
    });

    $(document).on('click', '.deleteData', function (e) {
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            buttons: {
                confirm: {
                    text: 'Yes, delete it!',
                    className: 'btn btn-success'

                },
                cancel: {
                    visible: true,
                    className: 'btn btn-danger'
                }
            }
        }).then((Delete) => {
            if (Delete) {
                var trid = $(this).data('trid');
                $.ajax({
                    url: '{{url()->current()}}/hapus/' + trid,
                    type: "GET",
                    dataType: 'json',
                    success: function (data) {
                        console.log('Sukses: ', data);
                        location.reload(true);  
                    },
                    error: function (data) {
                        console.log('Error: ', data);                        
                    }
                });
                swal({
                    title: 'Deleted!',
                    text: 'Your file has been deleted.',
                    type: 'success',
                    buttons: {
                        confirm: {
                            className: 'btn btn-success'
                        }
                    }
                });
            } else {
                swal.close();
            }
        });
    });
    
</script>
@endsection