{{-- @php
$data_session = Session::get('user_data');
$namaUser = $data_session['nama_pegawai'];
$bagian = $data_session['bagian'];
$level = $data_session['level'];
@endphp --}}
        <div class="sidebar sidebar-style-2" data-background-color="white">
            <div class="sidebar-wrapper scrollbar scrollbar-inner">
                <div class="sidebar-content">
                    <div class="user">
                        <div class="avatar-sm float-left mr-2">
                            <img src="{{asset('assets/img/profile.jpg')}}" alt="..." class="avatar-img rounded-circle">
                        </div>
                        <div class="info">
                            <a data-toggle="collapse" href="#collapseExample" aria-expanded="true">
                                <span>
									Arya Hafizh Tofani
                                <span class="user-level">Dev</span>
                                <span class="caret"></span>
                                </span>
                            </a>
                            <div class="clearfix"></div>

                            <div class="collapse in" id="collapseExample">
                                <ul class="nav">
                                    <li>
                                    <a data-toggle="collapse" href="#aa" aria-expanded="true" >
                                        <span>
                                            <span class="link-collapse">My Profile</span>
                                            <span class="caret"></span>
                                        </span>
                                    </a>   
                                    <div class="collapse in mt--4" id="aa">
                                        <ul class="nav pt-0 ml-3" style="color: #3d3d3d">
                                        <li>dasassa</li>
                                        <li>dasassa</li>
                                        <li>dasassa</li>
                                        </ul>
                                    </div>                                    
                                    </li>
                                    <li>
                                        <a href="#edit">
                                            <span class="link-collapse">Edit Profile</span>
                                        </a>
                                    </li>
                                    <li>
                                    <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                        <span class="link-collapse">Logout</span>
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>

                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <ul class="nav nav-primary">
                        <li class="nav-item {{ (request()->is('*paket')) ? 'active' : '' }} ">
                            <a href="{{url('paket')}}">
                                <i class="fas phpdebugbar-fa-check-circle"></i>
                                <p>Paket</p>
                            </a>
                        </li>
                        <li class="nav-item {{ (request()->is('*customer')) ? 'active' : ''}} ">
                            <a href="{{url('customer')}}">
                                <i class="fas phpdebugbar-fa-check-circle"></i>
                                <p>Customer</p>
                            </a>
                        </li>
                        <li class="nav-item {{ (request()->is('*transaksi')) ? 'active' : ''}} ">
                            <a href="{{url('transaksi')}}">
                                <i class="fas phpdebugbar-fa-check-circle"></i>
                                <p>Transaksi</p>
                            </a>
                        </li>
                        {{-- <li class="nav-item {{ (request()->is('*hero')) ? 'active' : '' }} ">
                            <a href="{{url('hero')}}">
                                <i class="fas phpdebugbar-fa-check-circle"></i>
                                <p>Hero</p>
                            </a>
                        </li>
                        <li class="nav-item {{ (request()->is('*skin')) ? 'active' : ''}} ">
                            <a href="{{url('skin')}}">
                                <i class="fas phpdebugbar-fa-check-circle"></i>
                                <p>Skin</p>
                            </a>
                        </li>
                        <li class="nav-item {{ (request()->is('*role')) ? 'active' : ''}} ">
                            <a href="{{url('role')}}">
                                <i class="fas phpdebugbar-fa-check-circle"></i>
                                <p>Role</p>
                            </a>
                        </li> --}}
                    </ul>
                </div>
            </div>
        </div>

        <!-- End Sidebar -->