<!-- Footer -->
<footer class="footer bg-grey2" style="position: fixed; bottom: 0; width: 100%;">
    <div class="container-fluid">
        <nav class="pull-left">
            <ul class="nav">
                <li class="nav-item">
                    2018 | made with <i class="fa fa-heart heart text-danger"></i> by DeSh4dow Team
                </li>
            </ul>
        </nav>
    </div>
</footer>