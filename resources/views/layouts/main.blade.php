<!DOCTYPE html>
<html lang="en">

<head>
    @include('layouts.header')
<style>
    .centerr {
    margin: 0;
    position: absolute;
    top: 50%;
    left: 50%;
    margin-right: -50%;
    transform: translate(-50%, -50%) }
    
    .scroll {
        max-height: 430px;
        overflow-y: auto;
    }    

    #preview {
        width: 100%;
        height: 250;
        margin: 0px auto;
    }

</style>    

</head>

<body>
    <div class="wrapper">
        @include('layouts.navbar')
        @include('layouts.sidebar')

        <!-- Main Content -->
        <div class="main-panel">

            <!-- Content -->
            <div class="content mb-5" id="particles-js" style="position: absolute; max-height: 630px;">
                @yield('content')                
            </div>

            @include('layouts.footer')
        </div>s
        
    </div>
</body>

@include('layouts.script')

@stack('scripts')
</html>