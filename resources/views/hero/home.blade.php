@extends('layouts.main')

@section('content')

<div class="panel-header bg-primary-gradient">
    <div class="page-inner py-5">
        <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
            <div class="col-12 text-center">
                <h2 class="text-white pb-2 fw-bold">List Data HERO MOBILE LEGENDs</h2>               
            </div>
        </div>
    </div>
</div>

<div class="page-inner mt--5 pb-0 w-100" style="position: absolute;">
    <div class="row mt--2">
        <div class="col-md-12">  

            <div class="card">
                {{-- atas --}}
                <div class="card-header">
                    <div class="card-head-row card-tools-still-right">
                        <h4 class="card-title">List Hero</h4>

                    </div>
                </div>
    
                {{-- isi table --}}
                <div class="card-body scroll">
                    
                    <div class="accordion accordion-secondary">
                    @foreach ($dataHero as $data)
                        
                        <div class="card">
                            <div class="card-header collapsed" id="headingThree" data-toggle="collapse" data-target="#{{$data['heroid']}}" aria-expanded="false" aria-controls="{{$data['heroid']}}">
                                <div class="span-icon">
                                    <i class="fa fa-angry"></i>
                                </div>
                                <div class="span-title">
                                    {{$data['heronama']}}
                                </div>
                                <div class="span-mode">
                                    <div class="pull-left mr-4">
                                    
                                    </div>
                                </div>
                            </div>
                            <div id="{{$data['heroid']}}" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                <div class="card-body row">
                                    <div class="col-md-6">
                                        <h3>Role</h3>
                                        <hr>
                                        @foreach ($data['role'] as $item)                                            
                                        <div class="row mb-3">
                                            <div class="col-md-12">
                                                {{$item}}
                                            </div>
                                        </div>
                                        @endforeach

                                    </div>
                                    <div class="col-md-6 table-responsive-md">
                                        <h3>Skin</h3>
                                        <hr>
                                        @foreach ($data['skin'] as $skin)                                            
                                        <div class="row mb-3">
                                            <div class="col-md-12">
                                                {{$skin->SKINNAMA}}
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach

                    </div>


                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modalgejala" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header no-bd">
                <h5 class="modal-title">
                    <span class="fw-mediumbold" id="title"></span>                    
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="formGejala"  name="formGejala">
                    @csrf
                    <input id="id_gejala" hidden type="text" name="id_gejala" class="form-control" value="">
                    <div class="row">
                        <div class="col-md-6 pr-0">
                            <div class="form-group form-group-default">
                                <label>Kode Gejala</label>
                                <input id="kode_gejala" name="kode_gejala" type="text" class="form-control"
                                    placeholder="Contoh: G001">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer no-bd">
                    <button type="reset" class="btn btn-danger" data-dismiss="modal">Tutup</button>
                    <button type="button" id="btnSave" class="btn btn-primary">Simpan</button>
                </div>
            </form>            
        </div>
    </div>
</div>


<div class="modal fade " id="pengajuan_ditolak" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog " style="width: 300px;" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Catatan:</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form id="formTolakPengajuan" name="formTolakPengajuan">
            <div class="modal-body">
            @csrf
            <input hidden name="id_penghargaan" id="id_penghargaan">
            <div class="form-group row" >
                <div class="col-md-12">
                    <label class="form-control-label mb-0" for="NamaUniv"></label>
                    <textarea class="form-control" name="catatan" id="catatan" rows="3" style="margin-top: 0px; margin-bottom: 0px; height: 100px;"></textarea>
                    <div class="invalid-feedback">*wajib diisi</div> 
                </div>
            </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" id="btnTolak" class="btn btn-primary">Simpan </button>
            </div>
        </form>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script type="text/javascript">

$('#pengajuan_ditolak').on('loaded.bs.modal', function (e) {
    console.log('anjay');
    $( "#formTolakPengajuan" ).validate( {
        rules: {
            catatan: "required",
        },
        messages: {
            catatan: {
                required: "tidak boleh kosong"
            }
        },
        errorElement: "em",
        errorPlacement: function ( error, element ) {
            // Add the `invalid-feedback` class to the error element
            error.addClass( "invalid-feedback" );
            if ( element.prop( "type" ) === "checkbox" ) {
                error.insertAfter( element.next( "label" ) );
            } else {
                error.insertAfter( element );
            }
        },
        highlight: function ( element, errorClass, validClass ) {
            $( element ).addClass( "is-invalid" ).removeClass( "is-valid" );

        },
        unhighlight: function (element, errorClass, validClass) {
            $( element ).addClass( "is-valid" ).removeClass( "is-invalid" );
        }
    } ); 
})

$(document).ready(function () {


});

$(function () {
    var url = "{{ url('admin/approval') }}"
    console.log(url);
    var content = '';
    
    // $.get(url, function (datas) {
    //     $.each(datas, function (i, datas) {
    //         console.log(datas);
    //         console.log(replace(datas.nomor_plat));
    //         content += 'hahahihi';
    //         $('.card-body').html(content);
    //         return;
    //     });
    // });

       

});

$('#btnTolak').click(function(ok) {



    if ($('#keluhan').hasClass('is-invalid')==true) {
        }else{
            ok.preventDefault();
            console.log('tol');

            if($('#srv_rutin').is(':checked')){
                $('#kfn_rutin').html('iya');
            }else{
                $('#kfn_rutin').html('tidak');
            }

            $('#kfn_keluhan').html($('#keluhan').val())

            // e.stopImmediatePropagation()
                $('#myModal').modal('show');
        }

});

function replace(params) {
    return (params.replace(' ','')).replace(' ','')
}
</script>
@endpush