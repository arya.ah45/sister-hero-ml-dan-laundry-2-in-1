@extends('layouts.main')

@section('content')

<div class="panel-header bg-primary-gradient">
    <div class="page-inner py-5">
        <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
            <div class="col-12 text-center">
                <h2 class="text-white pb-2 fw-bold">MOBILE LEGENDs Data's</h2>               
            </div>
        </div>
    </div>
</div>

<div class="page-inner mt--5 pb-0 w-100" style="position: absolute;">
    <div class="row mt--2">
        <div class="col-md-12">    
            <div class="card">
                {{-- atas --}}
                <div class="card-header">
                    <div class="card-head-row card-tools-still-right">
                        <h4 class="card-title">Data Role Hero </h4>
                        <div class="card-tools">
                            <button class="btn btn-primary btn-round ml-auto btnTambah" data-toggle="modal"
                                data-target="#modalpenyakit"><i class="fa fa-plus"></i>&nbsp Tambah Data</button>
                        </div>
                    </div>
                </div>
    
                {{-- isi table --}}
                <div class="card-body scroll">
                    <div class="table-responsive">
                        <table id="table_gejala" class="display table table-striped table-hover" >
                            <thead>
                                <tr>
                                    <th width="2%">No</th>
                                    <th width="10%">Role</th>
                                    <th width="10%">Nama</th>
                                    <th width="5%">Harga</th>
                                    <th width="15%" align="center">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($dataRole as $keys => $item)
                                <tr>
                                    <td>{{$keys+1}}</td>
                                    <td>{{$item->ROLENAMA}}</td>
                                    <td>{{$item->HERONAMA}}</td>
                                    <td>{{$item->HEROHARGA}}</td>       
                                    <td><a href="#" data-heroid="{{$item->HEROID}}" data-roleid="{{$item->ROLEID}}" class="btn btn-danger btn-sm deleteData">HAPUS</a> </td>       
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modalpenyakit" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header no-bd">
                <h5 class="modal-title">
                    <span class="fw-mediumbold" id="title"></span>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="formPenyakit"  name="formPenyakit">
                    @csrf
                    <input id="id" hidden type="text" name="id" class="form-control" value="">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group form-group-default">
                                <label>Pilih Hero</label>
                                <select class="form-control" name="hero_id" id="hero_id">
                                    @foreach ($optHero as $item)
                                    <option value="{{$item->HEROID}}">{{$item->HERONAMA}}</option>
                                    @endforeach                                    
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group form-group-default">
                                <label>Pilih Role</label>
                                <select class="form-control" name="role_id" id="role_id">
                                    @foreach ($optRole as $item)
                                    <option value="{{$item->ROLEID}}">{{$item->ROLENAMA}}</option>
                                    @endforeach                                    
                                </select>
                            </div>
                        </div>
                    </div>
            </div>
                <div class="modal-footer no-bd">
                    <button type="reset" class="btn btn-danger" data-dismiss="modal">Tutup</button>
                    <button type="button" id="btnSave" class="btn btn-primary">Tambah</button>
                </div>
            </form>
            
        </div>
    </div>
</div>

<script>

    $('.btnTambah').click(function() {
        $('#title').html('Tambah Role Hero');
        $('#modalpenyakit').modal({
            backdrop: 'static',
            keyboard: false, // to prevent closing with Esc button (if you want this too)
            show: true
        })
    });

    $('#btnSave').click(function (e) {
        e.preventDefault();
        $(this).html('Sending..');
        $.ajax({
            data: $('#formPenyakit').serialize(),
            url: "{{route('simpanRole')}}",
            type: "POST",
            dataType: 'json',
            success: function (data) {
                $('#formPenyakit').trigger('reset');
                $('#btnSave').html('Simpan');
                $('#modalpenyakit').modal('hide');
                location.reload(true);  
            },
            error: function (data) {
                console.log('Error:', data);
                $('#btnSave').html('Simpan');
            }
        });
    });

    $(document).on('click', '.deleteData', function (e) {
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            buttons: {
                confirm: {
                    text: 'Yes, delete it!',
                    className: 'btn btn-success'

                },
                cancel: {
                    visible: true,
                    className: 'btn btn-danger'
                }
            }
        }).then((Delete) => {
            if (Delete) {
                var heroid = $(this).data('heroid');
                var roleid = $(this).data('roleid');
                $.ajax({
                    url: '{{url()->current()}}/hapus/' + heroid + '/' + roleid,
                    type: "GET",
                    dataType: 'json',
                    success: function (data) {
                        console.log('Sukses: ', data);
                        location.reload(true);  
                    },
                    error: function (data) {
                        console.log('Error: ', data);                        
                    }
                });
                swal({
                    title: 'Deleted!',
                    text: 'Your file has been deleted.',
                    type: 'success',
                    buttons: {
                        confirm: {
                            className: 'btn btn-success'
                        }
                    }
                });
            } else {
                swal.close();
            }
        });
    });
    
</script>
@endsection